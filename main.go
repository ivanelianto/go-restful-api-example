package main

import (
	"fmt"
	"net/http"
	"log"
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
)

func setupResponse(w *http.ResponseWriter, r *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
    (*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
    (*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func Index(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)

	db := new(Database)
	db.connect()

	data, _ := db.Query("SELECT * FROM User")
	
	for data.Next() {
		var user User

		err := data.Scan(&user.ID, &user.Initial, &user.Name)
		if err != nil {
			log.Panic(err.Error())
		}

		bytes, err := json.Marshal(user)
		
		fmt.Fprintf(w, string(bytes))
	}
}

func Show(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	
	queryStringParams := mux.Vars(r)
	id := queryStringParams["id"]

	db := new(Database)
	db.connect()

	sql := fmt.Sprintf("SELECT * FROM User WHERE ID = %s", id)

	data, _ := db.Query(sql)
	
	for data.Next() {
		var user User

		err := data.Scan(&user.ID, &user.Initial, &user.Name)
		if err != nil {
			log.Panic(err.Error())
		}

		bytes, err := json.Marshal(user)
		
		fmt.Fprintf(w, string(bytes))
	}
}

func Store(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)

	requestBody, _ := ioutil.ReadAll(r.Body)

	var user User
	json.Unmarshal(requestBody, &user)

	db := new(Database)
	db.connect()

	sql := fmt.Sprintf("INSERT INTO User (Initial, Name) VALUES ('%s', '%s')", user.Initial, user.Name)
	db.Query(sql)
}

func Update(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)

	queryStringParams := mux.Vars(r)
	id := queryStringParams["id"]

	requestBody, _ := ioutil.ReadAll(r.Body)

	var user User
	json.Unmarshal(requestBody, &user)

	db := new(Database)
	db.connect()

	sql := fmt.Sprintf("UPDATE User SET Initial='%s', Name='%s' WHERE ID='%s'", user.Initial, user.Name, id)
	db.Query(sql)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)

	queryStringParams := mux.Vars(r)
	id := queryStringParams["id"]

	db := new(Database)
	db.connect()

	sql := fmt.Sprintf("DELETE FROM User WHERE ID='%s'", id)
	db.Query(sql)
}

func handleRequests() {
	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/users", Store).Methods("POST")
	router.HandleFunc("/users", Index)
	router.HandleFunc("/users/{id}", Update).Methods("PATCH")
	router.HandleFunc("/users/{id}", Delete).Methods("DELETE")
	router.HandleFunc("/users/{id}", Show)
	log.Fatal(http.ListenAndServe(":8081", router))
}

func main() {
	handleRequests()	
}