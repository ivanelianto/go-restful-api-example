package main

import (
	"fmt"
	"database/sql"
	"log"
	_ "github.com/go-sql-driver/mysql"
)

type Database struct {
	Connection *sql.DB
}

const Username string = "root"
const Password string = ""
const ServerAddress string = "127.0.0.1"
const ServerPort string = "3306"
const DatabaseName string = "slc"

func (db *Database) connect() *sql.DB {
	if (db.Connection == nil) {
		ConnectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", Username, Password, ServerAddress, ServerPort, DatabaseName)
		conn, err := sql.Open("mysql", ConnectionString)
		db.Connection = conn

		if err != nil {
			log.Fatal(err)
		}
	}

	return db.Connection
}

func (db *Database) Query(sql string) (*sql.Rows, error) {
	return db.Connection.Query(sql)
}